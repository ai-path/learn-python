"""Bot saves princess"""

def displayPathtoPrincess(n, grid):
    """Display bot path to rescue the princess in the grid"""
    m = []
    p = []
    for i in xrange(n):
        if "m" in grid[i]:
            m.append(i)
            m.append(grid[i].index("m"))

        if "p" in grid[i]:
            p.append(i)
            p.append(grid[i].index("p"))

    while m != p:
        if m[0] < p[0]:
            m[0] += 1
            print "DOWN"
        elif m[0] > p[0]:
            m[0] -= 1
            print "UP"
        elif m[1] < p[1]:
            m[1] += 1
            print "RIGHT"
        elif m[1] > p[1]:
            m[1] -= 1
            print "LEFT"

m_m = input()

m_grid = []
for i in xrange(0, m_m):
    m_grid.append(raw_input().strip())

displayPathtoPrincess(m_m, m_grid)
