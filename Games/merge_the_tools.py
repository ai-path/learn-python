"""Merge the tools!"""
import textwrap

def solution(string, size):
    """Split the string in substring of size n, prints each slide with unique characters"""
    tlist = textwrap.wrap(string, size)

    for sub in tlist:
        unique = "".join(reduce(lambda l, i: l.append(i) or l if i not in l else l, sub, []))
        print unique


solution('AABCAAADA', 3)
