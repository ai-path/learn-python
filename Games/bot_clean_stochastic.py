"""Bot Clean Stochastic"""
def next_move(posr, posc, board):
    """ Return next move of the bot """
    bot = [posr, posc]
    objective = []

    for i in xrange(5):
        for j in xrange(5):
            if board[i][j] == 'd':
                objective = [i, j]
                break

    if bot == objective:
        print "CLEAN"
    elif bot[0] < objective[0]:
        print "DOWN"
    elif bot[0] > objective[0]:
        print "UP"
    elif bot[1] < objective[1]:
        print "RIGHT"
    elif bot[1] > objective[1]:
        print "LEFT"

pos = [int(i) for i in raw_input().strip().split()]
board = [[j for j in raw_input().strip()] for i in range(5)]
next_move(pos[0], pos[1], board)