"""Bot saves princess - 2"""

def nextMove(n, r, c, grid):
    """Display next bot move to rescue the princess in the grid"""
    m = [r, c]
    p = []

    for i in xrange(n):
        if "p" in grid[i]:
            p.append(i)
            p.append(grid[i].index("p"))
            break

    if m[0] < p[0]:
        return "DOWN"
    elif m[0] > p[0]:
        return "UP"
    elif m[1] > p[1]:
        return "LEFT"
    elif m[1] < p[1]:
        return "RIGHT"

m_n = input()
m_r, m_c = [int(i) for i in raw_input().strip().split()]
m_grid = []
for i in xrange(0, m_n):
    m_grid.append(raw_input())

print nextMove(m_n, m_r, m_c, m_grid)
