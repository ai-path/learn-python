"""Bot Clean"""
def simulate(pos_x, pos_y, dirty_cell):
    """ Simulates next moves of the bot to the dirty cell
        and return the number of moves to get to the cell """
    if tuple([pos_x, pos_y]) == dirty_cell:
        return 0
    elif pos_x < dirty_cell[0]:
        pos_x += 1
    elif pos_x > dirty_cell[0]:
        pos_x -= 1
    elif pos_y < dirty_cell[1]:
        pos_y += 1
    elif pos_y > dirty_cell[1]:
        pos_y -= 1

    return 1 + simulate(pos_x, pos_y, dirty_cell)

def next_move(posr, posc, board):
    """ Return next move of the bot """
    bot = [posr, posc]
    dirty_cells = {}

    for i in xrange(5):
        for j in xrange(5):
            if board[i][j] == 'd':
                dirty_cells[tuple([i, j])] = 100

    for cell in dirty_cells:
        dirty_cells[cell] = simulate(bot[0], bot[1], cell)

    sorted_cells = sorted(dirty_cells.items(), key=lambda x: (x[1], x[0]))
    objective = sorted_cells[0]

    if objective[1] == 0:
        print "CLEAN"
    elif bot[0] < objective[0][0]:
        print "DOWN"
    elif bot[0] > objective[0][0]:
        print "UP"
    elif bot[1] < objective[0][1]:
        print "RIGHT"
    elif bot[1] > objective[0][1]:
        print "LEFT"

pos = [int(i) for i in raw_input().strip().split()]
board = [[j for j in raw_input().strip()] for i in range(5)]
next_move(pos[0], pos[1], board)

"""
0 0
bd---
-d---
---d-
--d-d
---d-
"""