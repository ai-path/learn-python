"""range vs xrange (Python 2.x): Simple test to see the performance difference"""
import time

range_size = 10000000

start = time.clock()
for x in range(range_size):
    pass
stop = time.clock()

print "range: %s" % (stop - start)

start = time.clock()
for x in xrange(range_size):
    pass
stop = time.clock()

print "xrange: %s" % (stop - start)
