"""Day 3: Conditionals"""

number = int(raw_input().strip())

if number % 2 != 0:
    print "Weird"
elif number in range(2, 6):
    print "Not Weird"
elif number in range(6, 21):
    print "Weird"
else:
    print "Not Weird"
