"""Day 28: RegEx, Patterns, and Intro to Databases"""
import re

my_dir = []
pattern = re.compile('\@gmail.com')

cases = int(raw_input().strip())
for _ in xrange(cases):
    firstName, emailID = raw_input().strip().split(' ')

    if pattern.search(emailID) is not None:
        my_dir.append(firstName)

my_dir.sort()

for name in my_dir:
    print name
