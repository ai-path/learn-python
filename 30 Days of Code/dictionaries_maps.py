"""Day 8: Dictionaries and Maps"""
class Agenda(object):
    """ Creates an agenda """
    agenda = {}
    def __init__(self, phone_numbers):
        self.agenda = dict(contact.split(' ') for contact in phone_numbers)

    def contact_info(self, contact):
        """ Search for a contact and return its info """
        return '%s=%s' % (contact, self.agenda.get(contact)) \
                if contact in self.agenda \
                else "Not found"

# Console test
"""
n = int(raw_input().strip())
phone_numbers = []

for x in xrange(n):
    phone_numbers.append(raw_input().strip())

agenda = Agenda(phone_numbers)

for i in xrange(n):
    print agenda.contact_info(raw_input().strip())
"""
