""" Day 10: Binary Numbers """

def all_on(number):
    """Given a base-10 int, return the correponding base-10 number of all 1's in given n"""
    return int(("".join(["1" for _ in bin(number)[2:]])), 2)

def max_on(number):
    """ Given a base-10 integer n, convert it to binary (base-2). Then find and print the base-10
    integer denoting the maximum number of consecutive 's in 's binary representation. """
    return len(max(("".join(bin(number)[2:])).split("0")))
    # Another solution counting 1s
    # return max(("".join(bin(number)[2:])).split('0')).count('1')

#print func(input())
