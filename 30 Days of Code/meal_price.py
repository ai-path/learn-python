"""Day 2: Meal price"""

def get_total_cost_of_meal(meal_cost, tip_percent, tax_percent):
    """Calculates the total cost of the meal.

    Args:
        meal_cost (float): Original meal price.
        tip_percent (int): Tip percentage.
        tax_percent (int): Tax percentage.
    """
    tip = meal_cost * (float(tip_percent) / 100)
    tax = meal_cost * (float(tax_percent) / 100)

    total_cost = int(round(meal_cost + tip + tax))
    return 'The total meal cost is %.2f dollars.' % total_cost

# To test in console, uncomment this line
#print get_total_cost_of_meal(float(raw_input()), float(raw_input()), float(raw_input()))
