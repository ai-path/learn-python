"""Day 17: More Exceptions"""

class Calculator(object):
    """Calculator class."""
    def __init__(self):
        pass

    def power(self, number, powa):
        """Raise an error if n or p is negative"""
        if number < 0 or powa < 0:
            raise Exception('n and p should be non-negative.')
        else:
            return number ** powa
