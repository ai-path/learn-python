# pylint: disable=R0903
"""Day 20: Sorting"""

class Solution(object):
    """Solution class"""

    total_swaps = 0
    first_element = 0
    last_element = 0

    def sort(self, size, arr):
        """Sort array using Buuble sort"""
        for _ in xrange(size):
            swaps = 0
            for index in xrange(size - 1):
                if arr[index] > arr[index + 1]:
                    item = arr[index + 1]
                    arr[index + 1] = arr[index]
                    arr[index] = item
                    swaps += 1

            self.total_swaps += swaps
            if swaps == 0:
                self.first_element = arr[0]
                self.last_element = arr[size - 1]
                break

        return arr
