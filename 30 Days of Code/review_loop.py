"""Day 6: Let's Review"""

def func(word):
    """Separate letters in word by odd and even"""
    even = ""
    odd = ""

    for index in range(0, len(word)):
        if index % 2 == 0:
            even = even + word[index]
        else:
            odd = odd + word[index]

    return "{} {}".format(even, odd)
