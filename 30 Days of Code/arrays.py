"""Day 7: Arrays"""

def func(list_input):
    """Return array in reverse order separated by space"""
    arr = map(int, list_input.split(' '))
    return " ".join(map(str, arr[::-1]))

# To test in console, uncomment this line
#print func(raw_input().strip())
