"""Day 9: Recursion"""

def factorial(number):
    """ Factorial function """
    return number * factorial(number - 1) if number >= 1 else 1


factorial_lambda = lambda n: 1 if n <= 1 else n * factorial_lambda(n - 1)

"""
while True:
    try:
        n = input()
        print "{}! = {}".format(n, factorial_lambda(n))
    except EOFError:
        break
"""
