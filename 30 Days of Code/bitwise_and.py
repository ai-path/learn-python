"""Day 29: Bitwise AND"""
import time

def solution(numbers, maximum):
    """Gets maximum value in the set of numbers that fits A&B"""
    elements = range(1, numbers + 1)

    res = 0

    for index in elements[:len(elements) - 1]:
        for jindex in elements[index:]:
            temp = index & jindex
            if temp == maximum - 1:
                return temp

            if temp > res and temp < maximum:
                res = temp

    return res

def solution_fast(serie, maximum):
    """Gets maximum value in the set of numbers that fits A&B"""
    """
        Explanation:

            serie = n
            maximum = k

        When k is ODD , k-1 is EVEN , k-1 can always be reached by (k-1) & k.

        In binary form:
            k   = 11
            k-1 = 10
            k-1 == (k-1) & k

        That is , ((k-1) | k) is always k. And ((k-1) | k) <= n is always TRUE.

        When k is EVEN, k-1 is ODD, k-1 can only be reached if and only if ((k-1) | k) <= n is TRUE

        Why?

        In binary form:
            k   = 10110
            k-1 = 10101
            pos = 10111
            k-1 == (k-1) & pos

        You can get k-1 if pos <= n is TRUE. And you can get pos by ((k-1) | (k-1+1)),
        that is, ((k-1) | k). Otherwise, you just need to follow the process above
        when k is ODD (because k-1 is ODD), then you get the answer k-2.

        In brief , you can just do the test ((k-1) | k) <= n to determine the answer.
        """
    return maximum - 1 if ((maximum - 1) | maximum) <= serie else maximum - 2

start = time.clock()
print solution(2501, 2501)
stop = time.clock()
print "solution: %s" % (stop - start)

start = time.clock()
print solution_fast(2501, 2501)
stop = time.clock()
print "fast: %s" % (stop - start)
