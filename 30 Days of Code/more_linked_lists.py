"""Day 24: More Linked Lists"""

class Node(object):
    """ Node class """
    def __init__(self, data):
        """Constructor."""
        self.data = data
        self.next = None

class Solution(object):
    """ Solution class """

    def display(self, head):
        """ Print linked list. """
        current = head
        while current:
            print current.data,
            current = current.next

    def get_list(self, head):
        """ Print linked list. """
        current = head
        data = []
        while current:
            data.append(current.data)
            current = current.next
        return data

    def insert(self, head, data):
        """Insert a node in the tree with the following rule:
            - If data is smaller than root data, go left
            - If data is bigger than root data, go right
        """
        node = Node(data)
        if head is None:
            head = node
        elif head.next is None:
            head.next = node
        else:
            start = head
            while start.next != None:
                start = start.next
            start.next = node
        return head

    def remove_duplicates(self, head):
        """Remove duplicates in the tree"""
        elements = []
        root = head
        while head:
            elements.append(head.data)
            if head.next is not None:
                if elements.count(head.next.data) > 0:
                    old_next = head.next
                    head.next = head.next.next
                    del old_next
                else:
                    head = head.next

            if head.next is None:
                break

        return root
