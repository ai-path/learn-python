"""Day 12: Inheritance"""

class Person(object):
    """ Person class """
    def __init__(self, firstName, lastName, idNumber):
        """ Base constructor.

        Args:
            firstName (str): A string denoting the Person's first name.
            lastName (str): A string denoting the Person's last name.
            idNumber (str): An integer denoting the Person's ID number.
        """
        self.first_name = firstName
        self.last_name = lastName
        self.id_number = idNumber

    def print_person(self):
        """Print person info"""
        print "Name:", self.last_name + ",", self.first_name
        print "ID:", self.id_number

class Student(Person):
    """Student class derived from Person"""

    def __init__(self, firstName, lastName, idNumber, scores):
        """ Student constructor.

        Args:
            firstName (str): A string denoting the Person's first name.
            lastName (str): A string denoting the Person's last name.
            idNumber (str): An integer denoting the Person's ID number.
            scores (arr): An array of integers denoting the Person's test scores.
        """
        super(Student, self).__init__(firstName, lastName, idNumber)
        self.scores = scores

    def calculate(self):
        """ A character denoting the grade. """
        average = sum(self.scores) / len(self.scores)
        if average >= 90:
            return 'O'
        elif average >= 80:
            return 'E'
        elif average >= 70:
            return 'A'
        elif average >= 55:
            return 'P'
        elif average >= 40:
            return 'D'
        else:
            return 'T'
