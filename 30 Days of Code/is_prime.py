"""Day 25: Running Time and Complexity (Is Prime)"""
import math

def is_int(number):
    """Validates if a given number is int"""
    absolute = abs(number)
    rounded = round(number)
    return absolute - rounded == 0

def is_prime(number):
    """Validates if a given number is prime"""
    if number <= 1:
        return False

    sqrt = int(math.sqrt(number))
    for serie in range(2, sqrt + 1):
        if number % serie == 0:
            return False
    return True
