"""Day 18: Queues and Stacks"""

class Solution(object):
    """Solution to solve palindrome"""
    def __init__(self):
        self.stack = []
        self.queue = []

    def push_character(self, char):
        """Pushes a character onto a Stack."""
        self.stack.append(char)

    def enqueue_character(self, char):
        """Enqueues a character in the Queue."""
        self.queue.append(char)

    def pop_character(self):
        """Pops and returns the character at the top of the Stack."""
        return self.stack.pop()

    def dequeue_character(self):
        """Dequeues and returns the first character in the Queue."""
        item = self.queue[0]
        self.queue.remove(item)
        return item

    def is_palindrome(self, word):
        """Determine if the word is a palindrome."""
        is_palindrome = True
        for index in xrange(len(word)):
            self.push_character(word[index])
            self.enqueue_character(word[index])

        for index in xrange(len(word) / 2):
            if self.pop_character() != self.dequeue_character():
                is_palindrome = False
                break

        return is_palindrome
