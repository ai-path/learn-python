# pylint: disable=R0903
"""Day 22: Binary Search Trees"""

class Node(object):
    """Node class"""
    def __init__(self, data):
        self.right = self.left = None
        self.data = data

class Solution(object):
    """Solution class"""

    def insert(self, root, data):
        """Insert a node in the tree with the following rule:
            - If data is smaller than root data, go left
            - If data is bigger than root data, go right
        """
        if root is None:
            return Node(data)
        else:
            if data <= root.data:
                cur = self.insert(root.left, data)
                root.left = cur
            else:
                cur = self.insert(root.right, data)
                root.right = cur
        return root

    def get_height(self, root):
        """Get of the BST"""
        return -1 if root is None else \
                max(self.get_height(root.right), self.get_height(root.left)) + 1
