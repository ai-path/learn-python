"""Day 4: Class vs Instance"""

class Person(object):
    """Person class"""
    def __init__(self, initialAge):
        if initialAge > 0:
            self.age = initialAge
        else:
            self.age = 0
            print "Age is not valid, setting age to 0."

    def year_passes(self):
        """Increase the Age by 1"""
        self.age += 1

    def am_i_old(self):
        """Validates age"""
        if self.age < 13:
            print "You are young."
        elif self.age >= 13 and self.age < 18:
            print "You are a teenager."
        else:
            print "You are old."
