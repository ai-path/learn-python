# pylint: disable=R0903
"""Day 13: Abstract Classes"""
from abc import ABCMeta, abstractmethod

class Book(object):
    """ Book abstract class. """
    __metaclass__ = ABCMeta
    def __init__(self, title, author):
        """ Book class constructor.

        Args:
            title (str): Book's Title
            author (str): Book's Author
            price (float): Book's price
        """
        self.title = title
        self.author = author

    @abstractmethod
    def display(self):
        """ Print's book info """
        pass

class MyBook(Book):
    """ My Book class. """
    def __init__(self, title, author, price):
        """ My book class constructor.

        Args:
            title (str): Book's Title
            author (str): Book's Author
            price (float): Book's price
        """
        super(MyBook, self).__init__(title, author)
        self.price = price

    def display(self):
        print "Title:", self.title
        print "Author:", self.author
        print "Price:", self.price
