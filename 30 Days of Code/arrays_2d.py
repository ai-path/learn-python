# pylint: disable=C0103
"""Day 11: 2D Arrays"""

def max_hourglass(arr):
    """Returns the sum of the max hourglass in the given 2D Array"""
    sums = []
    for x in xrange(len(arr) - 2):
        for y in xrange(len(arr) - 2):
            hourglass_sum = sum(arr[x][y:y+3]) + arr[x+1][y+1] + sum(arr[x+2][y:y+3])
            sums.append(hourglass_sum)
    return max(sums)
