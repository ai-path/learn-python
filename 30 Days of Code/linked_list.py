"""Day 15: Linked List"""

class Node(object):
    """ Node class """
    def __init__(self, data):
        """Constructor."""
        self.data = data
        self.next = None

class Solution(object):
    """ Solution class """

    def display(self, head):
        """ Print linked list. """
        current = head
        while current:
            print current.data,
            current = current.next

    def insert(self, head, data):
        """ Insert a new node into the linked list """
        if head is None:
            head = Node(data)
        else:
            head.next = self.insert(head.next, data)
        return head

"""
sol = Solution()
node = sol.insert(None, 4)
node = sol.insert(node, 2)
node = sol.insert(node, 3)
node = sol.insert(node, 4)
node = sol.insert(node, 1)
sol.display(node)
"""
