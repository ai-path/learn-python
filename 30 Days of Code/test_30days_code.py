"""Test cases for 30 days of code files"""

import datetime

import meal_price as day2
import review_loop as day6
import arrays as day7
import dictionaries_maps as day8
import recursion as day9
import binary_numbers as day10
import arrays_2d as day11
import inheritance as day12
import absolute_difference as day14
import exceptions_str_int as day16
import queueus_stacks as day18
import sorting as day20
import binary_search_tree as day22
import bst_level_order_traversal as day23
import more_linked_lists as day24
import is_prime as day25
import nested_logic as day26

# Test for Day 2: Meal price
def test_meal_price_1():
    """Test 1 - Day 2"""
    assert day2.get_total_cost_of_meal(12.00, 20, 8) \
            == 'The total meal cost is 15.00 dollars.'

# Tests for Day 6: Let's Review
def test_review_loop_1():
    """Test 1 - Day 6"""
    assert day6.func("Hacker") == "Hce akr"

def test_review_loop_2():
    """Test 2 - Day 6"""
    assert day6.func("Rank") == "Rn ak"

# Tests for Day 7: Arrays
def test_arrays_1():
    """Test 1 - Day 7"""
    assert day7.func("1 4 3 2") == "2 3 4 1"

def test_arrays_2():
    """Test 2 - Day 7"""
    assert day7.func("8 9 7 5 3 2 1") == "1 2 3 5 7 9 8"

# Tests for Day 8: Dictionaries and Maps
def test_agenda():
    """Test - Dictionaries and Maps"""
    agenda = day8.Agenda(["sam 99912222", "tom 11122222", "harry 12299933"])
    assert agenda.contact_info("sam") == "sam=99912222"
    assert agenda.contact_info("edward") == "Not found"
    assert agenda.contact_info("harry") == "harry=12299933"

# Tests for Day 9: Recursion
def test_factorial_1():
    """Test 1 - Factorial(recursive)"""
    assert day9.factorial(3) == 6

def test_factorial_2():
    """Test 2 - Factorial(recursive)"""
    assert day9.factorial(10) == 3628800

def test_factorial_lambda():
    """Test 3 - Factorial lambda"""
    assert day9.factorial_lambda(5) == 120

# Tests for Day 10: Binary numbers
def test_binary_numbers_all_on_1():
    """Test 1 - Binary all on """
    assert day10.all_on(4) == 7

def test_binary_numbers_all_on_2():
    """Test 2 - Binary all on """
    assert day10.all_on(30) == 31

def test_binary_numbers_all_on_3():
    """Test 3 - Binary all on """
    assert day10.all_on(100) == 127

def test_binary_numbers_max_on_1():
    """Test 1 - Binary max on """
    assert day10.max_on(5) == 1

def test_binary_numbers_max_on_2():
    """Test 2 - Binary max on """
    assert day10.max_on(13) == 2

# Tests for Day 11: 2D Array
def test_max_hourglass_1():
    """Tets 1 - Max hourglass """
    arr = [[1, 1, 1, 0, 0, 0],\
           [0, 1, 0, 0, 0, 0],\
           [1, 1, 1, 0, 0, 0],\
           [0, 0, 2, 4, 4, 0],\
           [0, 0, 0, 2, 0, 0],\
           [0, 0, 1, 2, 4, 0]]
    assert day11.max_hourglass(arr) == 19

# Tests for Day 12: Inheritance
def test_inheritance():
    """ Test - Inheritance """
    student = day12.Student("Heraldo", "Memelli", 8135627, [100, 80])
    assert student.calculate() == 'O'

# Test for Day 14: Scope
def test_scope():
    """ Test - Scope"""
    difference = day14.Difference([1, 2, 5])
    difference.compute_difference()
    assert difference.maximum_difference == 4

# Test for Day 16: Exceptions - String to Integer
def test_ex_str_int_1():
    """ Test 1 - Exceptions - String to Integer"""
    assert day16.func('3') == 3

def test_ex_str_int_2():
    """ Test 2 - Exceptions - String to Integer"""
    assert day16.func('za') == 'Bad String'

# Tests for Day 18: Queues and Stacks
def test_is_palindrome_1():
    """ Test 1 - Palindrome"""
    obj = day18.Solution()
    assert obj.is_palindrome('racecar') is True

def test_is_palindrome_2():
    """ Test 2 - Palindrome"""
    obj = day18.Solution()
    assert obj.is_palindrome('home') is False

# Test for Day 20: Sorting
def test_sorting_1():
    """ Test 1 - Sorting"""
    obj = day20.Solution()
    obj.sort(3, [1, 2, 3])
    assert obj.total_swaps == 0
    assert obj.first_element == 1
    assert obj.last_element == 3

def test_sorting_2():
    """ Test 2 - Sorting"""
    obj = day20.Solution()
    obj.sort(3, [3, 2, 1])
    assert obj.total_swaps == 3
    assert obj.first_element == 1
    assert obj.last_element == 3

# Test for Day 22: Binary Search Trees
def test_bst():
    """ Test BST """
    data = [3, 5, 2, 1, 4, 6, 7]
    root = None
    my_tree = day22.Solution()

    for dat in data:
        root = my_tree.insert(root, dat)

    assert my_tree.get_height(root) == 3

# Test for Day 23: BST Level-Order Traversal
def test_bst_level_order():
    """ Test BST Level-Order Traversal """
    data = [3, 5, 4, 7, 2, 1]
    root = None
    my_tree = day23.Solution()

    for dat in data:
        root = my_tree.insert(root, dat)

    assert my_tree.level_order(root) == "3 2 5 1 4 7"

# Test for Day 24: More Linked Lists
def test_more_linked_lists():
    """ Test More Linked Lists """
    data = [1, 2, 2, 3, 3, 4]
    root = None
    mylist = day24.Solution()

    for dat in data:
        root = mylist.insert(root, dat)

    root = mylist.remove_duplicates(root)
    assert mylist.get_list(root) == [1, 2, 3, 4]

# Tests for Day 25: Running Time and Complexity (Is Prime)
def test_is_prime_1():
    """Test 1 - Is Prime"""
    assert day25.is_prime(12) is False

def test_is_prime_2():
    """Test 2 - Is Prime"""
    assert day25.is_prime(100000003) is False

def test_is_prime_3():
    """Test 3 - Is Prime"""
    assert day25.is_prime(1000000007)

# Test Day 26: Nested Logic
def test_nested_logic():
    """Test - Day 26: Nested Logic"""
    r_date = datetime.date(2015, 6, 9)
    d_date = datetime.date(2015, 6, 6)
    assert day26.solution(r_date, d_date) == 45
