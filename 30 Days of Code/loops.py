"""Day 5: Loops"""

n = int(raw_input().strip())

for i in range(1, 11):
    print "{} x {} = {}".format(n, i, n * i)
