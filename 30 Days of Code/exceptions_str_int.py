"""Day 16: Exceptions - String to Integer"""

def func(text):
    """Try to convert a string to int"""
    try:
        number = int(text)
        return number
    except ValueError:
        return "Bad String"

#print func(raw_input().strip())
