"""Day 14: Scope"""

class Difference(object):
    """ Difference class """

    maximum_difference = 0
    def __init__(self, elements):
        """Constructor"""
        self.elements = elements

    def compute_difference(self):
        """Calculates maximum absolute difference between numbers in elements."""
        self.maximum_difference = max(self.elements) - min(self.elements)
