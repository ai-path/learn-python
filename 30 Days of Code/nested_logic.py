"""Day 26: Nested Logic"""

def solution(return_date, due_date):
    """Return debt # between 2 dates"""
    if return_date <= due_date:
        return 0
    elif return_date.year > due_date.year:
        return 10000
    elif return_date.month > due_date.month:
        return 500 * (return_date.month - due_date.month)

    return 15 * (return_date.day - due_date.day)
