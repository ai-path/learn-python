# pylint: disable=R0903, C1801, R0201
"""Day 23: BST Level-Order Traversal"""

class Node(object):
    """Node class"""
    def __init__(self, data):
        self.right = self.left = None
        self.data = data

class Solution(object):
    """Solution class"""

    def insert(self, root, data):
        """ Insert a node in the tree with the following rule:
            - If data is smaller than root data, go left
            - If data is bigger than root data, go right
        """
        if root is None:
            return Node(data)
        else:
            if data <= root.data:
                cur = self.insert(root.left, data)
                root.left = cur
            else:
                cur = self.insert(root.right, data)
                root.right = cur
        return root

    def level_order(self, root):
        """ Return the tree array in level order """
        if root is not None:
            nodes = []
            array_print = []
            nodes.append(root)

            while len(nodes) > 0:
                node = nodes[0]
                nodes.remove(node)

                array_print.append(node.data)

                if node.left is not None:
                    nodes.append(node.left)

                if node.right is not None:
                    nodes.append(node.right)

            return " ".join(map(str, array_print[::]))


#T = int(raw_input())
#myTree = Solution()
#my_root = None
#for i in range(T):
#    data = int(raw_input())
#    my_root = myTree.insert(my_root, data)
#print myTree.level_order(my_root)
